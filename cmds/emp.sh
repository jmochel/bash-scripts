#!/usr/bin/env bash

#
# FMPP Template Script
#

# Best practices
#
# 	http://mywiki.wooledge.org/BashGuide/Practices
# 
#

# Folder containing FMPP templates

TEMPLATE_DIR=$HOME/dev/canon-templates

# FMPP installation 

FMPP_DIR=$HOME/dev/fmpp

# Check for FMPP installation

if [[ ! -e "$FMPP_DIR" ]]; then  
	echo "FMPP Installation does not appear to exist ..."	
	echo "it was expected at $FMPP_DIR"
	exit 1
fi
                     

# Check for FMPP template installation

if [[ ! -e "$TEMPLATE_DIR" ]]; then  
	echo "FMPP Template directory does not appear to exist ..."	
	echo "it was expected at $TEMPLATE_DIR"
	exit 1
fi

# Functions

listTemplates() {

	for f in $TEMPLATE_DIR/* ; do 
			if [[ -d $f ]]; then 
				printf '%s\n' "${f##*/}"
			fi
	done
}

                       
USAGE="Usage: `basename $0` [-hv] [-f arg]"
 
filename=

# Command Line Parsing

while getopts hvl OPT
do
    case "$OPT" in
    	h)
    			echo $USAGE
    			exit 0
    			;;
      v)  
      		vflag=on
      		;;
      l)  
      		listTemplates
      		;;
      \?)
      		echo $USAGE >&2
	  			exit 1;;
    esac
done

# Remove the options we parsed above.

shift `expr $OPTIND - 1`

# EOF